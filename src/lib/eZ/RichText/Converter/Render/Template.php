<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render;

use ContextualCode\EzPlatformRichTextTemplateExtension\eZ\RichText\Converter\Render\Template\Collection\Extension as ExtensionsCollection;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Ibexa\Contracts\FieldTypeRichText\RichText\Converter;
use Ibexa\FieldTypeRichText\RichText\Converter\Render\Template as Base;
use Ibexa\Contracts\FieldTypeRichText\RichText\RendererInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * RichText Template converter injects rendered template payloads into template elements.
 */
class Template extends Base
{
    /**
     * @var Converter
     */
    private $richTextConverter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ExtensionsCollection
     */
    private $extensionsCollection;

    /**
     * RichText Template converter constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(
        RendererInterface $renderer,
        Converter $richTextConverter,
        ExtensionsCollection $extensionsCollection,
        LoggerInterface $logger = null
    ) {
        $this->richTextConverter = $richTextConverter;
        $this->extensionsCollection = $extensionsCollection;
        $this->logger = $logger ?? new NullLogger();

        parent::__construct($renderer, $richTextConverter, $logger);
    }

    /**
     * Overrided to fix issue with child templates processed multiple times.
     * Method can be removed after eZ fix.
     *
     * Injects rendered payloads into template elements.
     */
    public function convert(DOMDocument $document): DOMDocument
    {
        $xpath = new DOMXPath($document);
        $xpath->registerNamespace('docbook', 'http://docbook.org/ns/docbook');
        $xpathExpression = '//docbook:eztemplate[not(ancestor::docbook:eztemplate)] | //docbook:eztemplateinline[not(ancestor::docbook:eztemplateinline)]';

        foreach ($xpath->query($xpathExpression) as $template) {
            $this->processTemplate($document, $xpath, $template);
        }

        return $document;
    }

    /**
     * Processes given template $template in a given $document.
     */
    protected function processTemplate(DOMDocument $document, DOMXPath $xpath, DOMElement $template): void
    {
        $content = null;
        $templateName = $template->getAttribute('name');
        $templateType = $template->hasAttribute('type') ? $template->getAttribute('type') : 'tag';
        $parameters = [
            'name' => $templateName,
            'params' => $this->extensionsCollection->extend(
                $templateType,
                $templateName,
                $this->extractConfiguration($template)
            ),
        ];

        $contentNodes = $xpath->query('./docbook:ezcontent', $template);
        $innerContent = '';
        foreach ($contentNodes as $contentNode) {
            $innerContent .= $this->getCustomTemplateContent($contentNode);
        }
        if (!empty($innerContent)) {
            $parameters['content'] = $innerContent;
        }

        if ($template->hasAttribute('ezxhtml:align')) {
            $parameters['align'] = $template->getAttribute('ezxhtml:align');
        }

        $content = $this->renderer->renderTemplate(
            $templateName,
            $templateType,
            $parameters,
            $template->localName === 'eztemplateinline'
        );

        if ($content !== null) {
            $payload = $document->createElement('ezpayload');
            $payload->appendChild($document->createCDATASection($content));
            $template->appendChild($payload);
        }
    }

    /**
     * Extracts configuration hash from embed element.
     */
    protected function extractConfiguration(DOMElement $embed): array
    {
        $hash = [];

        $xpath = new DOMXPath($embed->ownerDocument);
        $xpath->registerNamespace('docbook', 'http://docbook.org/ns/docbook');
        $configElements = $xpath->query('./docbook:ezconfig', $embed);

        if ($configElements->length) {
            $hash = $this->extractHash($configElements->item(0));
        }

        if ($hash === null) {
            return [];
        }

        return $hash;
    }
}